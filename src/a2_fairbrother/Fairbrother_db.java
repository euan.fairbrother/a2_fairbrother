//--------------------------------------------------------------------
// Assignment 2, Euan Fairbrother, BITFT2a, Software Engineering
// Lecturer: Elzbieta Pustulka
//--------------------------------------------------------------------
package a2_fairbrother;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Fairbrother_db {
	
	public static void createNewTable() {
		// SQLite connection string
		String url = "jdbc:sqlite:C:/Users/euanf/Databases/Fairbrother.db";
		
		Connection conn = null;
		
		// SQL statement for creating a new table
		String sql = "CREATE TABLE IF NOT EXISTS births (\n"
				+ " id integer PRIMARY KEY,\n"
				+ " year text NOT NULL,\n"
				+ " canton text NOT NULL,\n"
				+ " age_mother text NOT NULL,\n"
				+ " sex_child text NOT NULL,\n"
				+ " obs_value text NOT NULL\n"
				+ ");";
		
		
		try {
			conn = DriverManager.getConnection(url);
			conn.setAutoCommit(false);
			System.out.println("Connection to SQLite has been established");
			
			Statement stmt = conn.createStatement();
			stmt.execute(sql);
			System.out.println(sql);
			
			conn.commit();			// these two lines have been added, hopefully they work
			conn.close();			//
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	// selectCount shows us how many rows are in the table
	public static void selectCount() {
		String sql = "select count(*) from births";
		String url = "jdbc:sqlite:C:/Users/euanf/Databases/Fairbrother.db";
		
		try {
			Connection conn = DriverManager.getConnection(url);
			
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			int count = rs.getInt(1);
			
			System.out.println("Number of records in the birth table: " + count);
			System.out.println("Closing connection to database");
			
			conn.close();
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void main(String[] args) {
		createNewTable();							// first we create the table
		
		String jdbcURL = "jdbc:sqlite:C:/Users/euanf/Databases/Fairbrother.db";
		String csvFilePath = "C:\\Users\\euanf\\eclipse-workspace\\a2_fairbrother\\Fairbrother.csv";
		
		int batchSize = 20;
		
		Connection connection = null;
		
		try {																		// then we fill the data in to the table
			connection = DriverManager.getConnection(jdbcURL);
			connection.setAutoCommit(false);
			
			String sql = "INSERT INTO births (year, canton, age_mother, sex_child, obs_value) VALUES (?, ?, ?, ?, ?)";
			PreparedStatement statement = connection.prepareStatement(sql);
			
			BufferedReader lineReader = new BufferedReader(new FileReader(csvFilePath));
			String lineText = null;
			
			int count = 0;
			
			lineReader.readLine(); // skip the header line
			
			while((lineText = lineReader.readLine()) != null) {			// This reads each line and each value within the line is assigned to the corresponding table column
				String[] data = lineText.split(";");
				String year = data[0];
				String canton = data[1];
				String age_mother = data[2];
				String sex_child = data[3];
				String obs_value = data[4];
				
				statement.setString(1, year);
				statement.setString(2, canton);
				statement.setString(3, age_mother);
				statement.setString(4, sex_child);
				statement.setString(5, obs_value);
				
				statement.addBatch();
				
				if (count % batchSize == 0) {
					statement.executeBatch();
				}
			}
			
			lineReader.close();
			
			// execute the remaining queries
			statement.executeBatch();
			
			connection.commit();
			connection.close();
		} catch (IOException ex) {
			System.err.println(ex);
		} catch (SQLException ex) {
			ex.printStackTrace();
			
			try {
				connection.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}	
		
		 selectCount();								// Finally we ask to see how many rows we have
	}

}
