package a2_fairbrother;
//--------------------------------------------------------------------
// Assignment 2, Euan Fairbrother, BITFT2a, Software Engineering
// Lecturer: Elzbieta Pustulka
//--------------------------------------------------------------------

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class Fairbrother_dbTest {

	@Test
	void test() throws IOException {
		String sql = "select count(*) from births";
		String url = "jdbc:sqlite:C:/Users/euanf/Databases/Fairbrother.db";
		

		 
		
		try (BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\euanf\\eclipse-workspace\\a2_fairbrother\\Fairbrother.csv"))) {
	
			Connection conn = DriverManager.getConnection(url);
	
	
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			int count = rs.getInt(1);
	
			
				List<List<String>> result = new ArrayList<>();
				String line;
				while ((line = br.readLine()) != null) {
					String[] values = line.split(",");
					result.add(Arrays.asList(values));
				}
			
			
			
				assertEquals(rs.getInt(1), result.size()-1);	// -1 for the header row
				
				conn.close();
			
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}

	}

